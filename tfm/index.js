const { app, BrowserWindow } = require('electron')
const path = require('path')

let pluginName
switch (process.platform) {
case 'win32':
  pluginName = 'pepflashplayer.dll'
  break
case 'darwin':
  pluginName = 'PepperFlashPlayer.plugin'
  break
case 'linux':
  pluginName = 'PepperFlash/libpepflashplayer.so'
  break
}
app.commandLine.appendSwitch('ppapi-flash-path', path.join(__dirname, pluginName))

app.commandLine.appendSwitch('ppapi-flash-version', '17.0.0.169')

app.on('ready', () => {
  let win = new BrowserWindow({
    titleBarStyle: 'hidden',
    width: 800,
    height: 600,
    icon: path.join(__dirname ,'images/Chatmane.ico'),
    webPreferences: {
      plugins: true
    }
  })
  win.loadURL(`file://${__dirname}/index.html`)
})
